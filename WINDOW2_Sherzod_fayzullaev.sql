WITH subcategory_sales AS (
    SELECT
        p.prod_subcategory,
        EXTRACT(YEAR FROM s.time_id) AS year,
        SUM(s.amount_sold) AS sales
    FROM
        sh.sales s
        JOIN sh.products p ON s.prod_id = p.prod_id
    WHERE
        EXTRACT(YEAR FROM s.time_id) IN (1998, 1999, 2000, 2001)
    GROUP BY
        p.prod_subcategory,
        EXTRACT(YEAR FROM s.time_id)
),
previous_year_sales AS (
    SELECT
        p.prod_subcategory,
        EXTRACT(YEAR FROM s.time_id) - 1 AS previous_year,
        SUM(s.amount_sold) AS sales
    FROM
        sh.sales s
        JOIN sh.products p ON s.prod_id = p.prod_id
    WHERE
        EXTRACT(YEAR FROM s.time_id) IN (1999, 2000, 2001, 2002)
    GROUP BY
        p.prod_subcategory,
        EXTRACT(YEAR FROM s.time_id) - 1
)
SELECT
    current_year.prod_subcategory
FROM
    subcategory_sales current_year
    JOIN previous_year_sales previous_year ON
        current_year.prod_subcategory = previous_year.prod_subcategory
WHERE
    current_year.sales < COALESCE(previous_year.sales, 0)
GROUP BY
    current_year.prod_subcategory
HAVING
    COUNT(DISTINCT current_year.year) = 3
   AND COUNT(DISTINCT previous_year.previous_year) >= 3;
