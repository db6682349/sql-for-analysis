WITH RankedCustomers AS (
    SELECT
        s.cust_id,
        c.cust_first_name || '   ' ||  c.cust_last_name AS customer_name,
        s.channel_id,
        ch.channel_desc,
        RANK() OVER (PARTITION BY s.channel_id ORDER BY SUM(s.amount_sold) DESC) AS sales_rank
    FROM
        sh.sales s
        INNER JOIN sh.customers c ON s.cust_id = c.cust_id
        INNER JOIN sh.channels ch ON s.channel_id = ch.channel_id
        INNER JOIN sh.times t ON s.time_id = t.time_id
    WHERE
        t.calendar_year IN (1998, 1999, 2001)
    GROUP BY
        s.cust_id, c.cust_first_name, c.cust_last_name, s.channel_id, ch.channel_desc
)
SELECT
    rc.cust_id,
    rc.customer_name,
    rc.channel_desc,
    SUM(s.amount_sold) AS total_sales
FROM
    RankedCustomers rc
    INNER JOIN sh.sales s ON rc.cust_id = s.cust_id AND rc.channel_id = s.channel_id
WHERE
    rc.sales_rank <= 300
GROUP BY
    rc.cust_id, rc.customer_name, rc.channel_desc
ORDER BY
    rc.channel_desc, total_sales DESC;
